/*
 *  Parts taken from 2.6.33 Kernel
 */

#ifndef __KFIFO_COMAPT_H__
#define __KFIFO_COMAPT_H__

#include <linux/kfifo.h>

/* Some backports from current kernel versions */
static inline __must_check int kfifo_is_empty(struct kfifo *fifo)
{
	return fifo->in == fifo->out;
}

static inline __must_check unsigned int kfifo_avail(struct kfifo *fifo)
{
	return fifo->size - kfifo_len(fifo);
}

static inline unsigned int __kfifo_off(struct kfifo *fifo, unsigned int off)
{
	return off & (fifo->size - 1);
}

static inline int __kfifo_to_user_data(struct kfifo *fifo, void __user *to, 
				       unsigned int len, unsigned int off, 
				       unsigned *lenout)
{
        unsigned int l;
        int ret;

        smp_rmb();

        off = __kfifo_off(fifo, fifo->out + off);

        l = min(len, fifo->size - off);
        ret = copy_to_user(to, fifo->buffer + off, l);
        *lenout = l;
        if (unlikely(ret)) {
                *lenout -= ret;
                return -EFAULT;
        }

        len -= l;
        ret = copy_to_user(to + l, fifo->buffer, len);
        if (unlikely(ret)) {
                *lenout += len - ret;
                return -EFAULT;
        }
        *lenout += len;

        return 0;
}

static inline void __kfifo_add_out(struct kfifo *fifo,
				   unsigned int off)
{
	smp_mb();
	fifo->out += off;
}

int kfifo_to_user(struct kfifo *fifo,
		  void __user *to, unsigned int len, unsigned *lenout)
{
	int ret;
	len = min(kfifo_len(fifo), len);
	ret = __kfifo_to_user_data(fifo, to, len, 0, lenout);
	__kfifo_add_out(fifo, *lenout);
	return ret;
}

#endif /* __KFIFO_COMAPT_H__ */
