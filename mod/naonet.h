/*
 * Naonet communication protocol
 *
 * Copyright (C) 2010  Daniel Borkmann <daniel@netsniff-ng.org>
 *
 * This program is free software; you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 2 of the License, or (at 
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with this program; if not, write to the Free Software Foundation, Inc., 
 * 51 Franklin St, Fifth Floor, Boston, MA 02110, USA
 */

#ifndef __NAONET_H__
#define __NAONET_H__

#include <linux/types.h>
#include <linux/wait.h>
#include <linux/kfifo.h>

/* Our Nao Ethernet type */
#define ETH_P_NAO  0xACDC

struct naohdr {
	__be16			len;
	__be32			src_id;
	__be16			opts;
};

/* Nao control structure */
struct naonet {
	rwlock_t 		ctl_lock;
	__be32			src_id;
	char			dev_name[IFNAMSIZ];
	size_t			rx_queue_elems;
	spinlock_t 		rx_queue_lock;
	struct kfifo *		rx_queue;
	wait_queue_head_t 	rx_queue_wait;
	rwlock_t 		rx_hook_lock;
	void 		      (*rx_hook)(struct naohdr *head, char *payload);
	atomic_t 		rx_frames_bq;
	atomic_t 		rx_frames_aq;
	atomic_t 		rx_frames_drop;
	atomic_t 		tx_frames;
};

#endif /* __NAONET_H__ */
