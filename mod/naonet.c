/*
 * Naonet IP-less Communication Protocol
 *
 * Implemented against 2.6.29 (Aldebaran Robotics current kernel 
 * version on Nao firmware). Code sucks a bit, needs some rewrite
 * here and there. IP-less communication from user space via files.
 *
 * Copyright (C) 2010  Daniel Borkmann <daniel@netsniff-ng.org>
 *
 * This program is free software; you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation; either version 2 of the License, or (at 
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with this program; if not, write to the Free Software Foundation, Inc., 
 * 51 Franklin St, Fifth Floor, Boston, MA 02110, USA
 */

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/skbuff.h>
#include <linux/netdevice.h>
#include <linux/if_ether.h>
#include <linux/in.h>
#include <linux/fs.h>
#include <linux/miscdevice.h>
#include <linux/proc_fs.h>
#include <linux/wait.h>
#include <linux/kfifo.h>
#include <linux/jiffies.h>
#include <asm/uaccess.h>

#include "naonet.h"
#include "kfifoc.h"

MODULE_DESCRIPTION("Naonet low-level ethernet protocol and file interface");
MODULE_AUTHOR("Daniel Borkmann <daniel@netsniff-ng.org>");
MODULE_LICENSE("GPL");

#define NAONET_PROC_DIR  "naonet"
#define NAONET_PROC_INF  "id"
#define NAONET_PROC_STAT "io"
#define NAONET_PROC_CTL  "ctl"
#define MAX_PARAM_LENGTH 256
#define DEV_NAME         "nao"

#ifdef __DEBUG__
#define debug(fmt, arg...)						\
		do {							\
			printk(KERN_DEBUG "naonet: %s (%d): " fmt,	\
			       __FUNCTION__, __LINE__, ## arg);		\
		} while(0);
#else
#define debug(fmt, arg...)						\
		do {} while(0);
#endif /* __DEBUG__ */

/* Global procfs entries */ 
static struct proc_dir_entry *naonet_dir;
static struct proc_dir_entry *naonet_inf;
static struct proc_dir_entry *naonet_ctl;
static struct proc_dir_entry *naonet_stat;

static struct naonet nn_control;
static char config[MAX_PARAM_LENGTH];

module_param_string(naonet, config, MAX_PARAM_LENGTH, 0);

MODULE_PARM_DESC(naonet, " naonet=[netdev],[src-id],[rxqueue-frames]");

static void naonet_print_options(struct naonet *nn)
{
	unsigned long flags;

	read_lock_irqsave(&nn->ctl_lock, flags);
	
	printk(KERN_INFO "naonet: net device: %s\n", nn->dev_name);
	printk(KERN_INFO "naonet: src id: %u\n", nn->src_id);
	printk(KERN_INFO "naonet: rx_queue size: %u frames\n", 
	       nn->rx_queue_elems);

	read_unlock_irqrestore(&nn->ctl_lock, flags);
}

static void naonet_init_struct(struct naonet *nn)
{
	nn->ctl_lock = __RW_LOCK_UNLOCKED(ctl_lock);
	nn->rx_queue_lock = __SPIN_LOCK_UNLOCKED(rx_queue_lock);
	nn->rx_hook_lock = __RW_LOCK_UNLOCKED(rx_hook_lock);
	nn->rx_hook = NULL;

	init_waitqueue_head(&nn->rx_queue_wait);
}

static void naonet_set_default_options(struct naonet *nn)
{
	unsigned long flags;
	struct net_device *dev = NULL;

	write_lock_irqsave(&nn->ctl_lock, flags);

	strlcpy(nn->dev_name, "wlan0", sizeof(nn->dev_name));
	dev = dev_get_by_name(&init_net, nn->dev_name);
	if(dev == NULL) {
		printk(KERN_INFO "naonet: Warning! Using random time instead "
		       "of HW address!\n");
		nn->src_id = (__u32) jiffies;
	} else {
		memcpy(&nn->src_id, &dev->dev_addr[3], 3);
		nn->src_id <<= 8;
		/* nn->src_id = cpu_to_be32(nn->src_id); */
	}
	nn->rx_queue_elems = 1000;

	write_unlock_irqrestore(&nn->ctl_lock, flags);
}

static int __must_check naonet_parse_options(struct naonet *nn, char *opt)
{
	char *cur = opt, *delim;
	/* No locking needed at this stage */

	/* Parse device name */
	if(*cur != ',') {
		if((delim = strchr(cur, ',')) == NULL)
			goto err;
		*delim = 0;
		strlcpy(nn->dev_name, cur, sizeof(nn->dev_name));
		cur = delim;
	}
	cur++;

	/* Parse src id */
	if(*cur != ',') {
		if((delim = strchr(cur, ',')) == NULL)
			goto err;
		*delim = 0;
		nn->src_id = simple_strtol(cur, NULL, 10);
		cur = delim;
	}
	cur++;

	/* Parse queue size */
	if(*cur != 0) {
		nn->rx_queue_elems = simple_strtol(cur, NULL, 10);
	}

	return 0;
err:
	printk(KERN_INFO "naonet: couldn't parse config at \'%s\'!\n", cur);
	return -1;
}

void debug_hook(struct naohdr *header, char *payload)
{
	payload[ntohs(header->len)] = 0;

	debug("head->len:     %u\n", ntohs(header->len));
	debug("head->sid:     %x\n", header->src_id); 
	debug("head->opts:    %x\n", header->opts); 
	debug("payload:       %s\n", payload); 
}

static inline struct naohdr *naonet_fetch_header(const struct sk_buff *skb)
{
	return (struct naohdr *) skb_network_header(skb);
}

static inline char *naonet_fetch_payload(const struct sk_buff *skb)
{
	return (char *) (skb_network_header(skb) + sizeof(struct naohdr));
}

static int __nao_send(struct naonet *nn, const char *buff, size_t buff_len, 
		      uint8_t opts, uint16_t type)
{
	int rc;
	size_t len = 0, nethdr_len = 0;
	uint8_t *buff_cpy = NULL;
	unsigned long flags;

	struct sk_buff *skb = NULL;
	struct net_device *dev = NULL;
	struct ethhdr *l2h = NULL;
	struct naohdr *naoh = NULL;

	if(unlikely((buff_len == 0) || buff == NULL || nn == NULL))
		return -EINVAL;

	read_lock_irqsave(&nn->ctl_lock, flags);

	dev = dev_get_by_name(&init_net, nn->dev_name);
	if(dev == NULL) {
		rc = -ENOENT;
		goto err;
	}
	if(unlikely(buff_len > dev->mtu)) {
		rc = -EMSGSIZE;
		goto err;
	}
	if(!netif_device_present(dev) || !netif_running(dev)) {
		rc = -EIO;
		goto err;
	}

	len += LL_RESERVED_SPACE(dev);
	len += sizeof(struct ethhdr);
	len += sizeof(struct naohdr);
	len += buff_len;

	skb = alloc_skb(len, GFP_ATOMIC);
	if(skb == NULL) {
		rc = -ENOMEM;
		goto err;
	}

	skb_reserve(skb, LL_RESERVED_SPACE(dev));
	skb->dev = dev;

	l2h = (struct ethhdr *) skb_put(skb, sizeof(*l2h));
	l2h->h_proto = skb->protocol = cpu_to_be16(ETH_P_NAO);

	memset(l2h->h_dest, 0xFF, ETH_ALEN);
	memcpy(l2h->h_source, dev->dev_addr, ETH_ALEN);

	nethdr_len += sizeof(*l2h);
	skb_set_network_header(skb, nethdr_len);

	naoh = (struct naohdr *) skb_put(skb, sizeof(*naoh));

	naoh->len = cpu_to_be16(buff_len);
	naoh->src_id = nn->src_id;
	naoh->opts = opts;

	skb_set_transport_header(skb, len - LL_RESERVED_SPACE(dev) - buff_len);

	buff_cpy = skb_put(skb, buff_len);
	rc = copy_from_user(buff_cpy, buff, buff_len);
	if(rc) {
		printk(KERN_ERR "naonet: error copy from user!\n");
		goto err;
	}

	read_unlock_irqrestore(&nn->ctl_lock, flags);

	atomic_inc(&nn->tx_frames);
	/* Finish him! */
	return dev_queue_xmit(skb);
err:
	read_unlock_irqrestore(&nn->ctl_lock, flags);
	return rc;
}

int naonet_send(struct naonet *nn, const char *buff, size_t buff_len)
{
	return  __nao_send(nn, buff, buff_len, 0, 0);
}

static inline void naonet_consume_skb(struct sk_buff *skb)
{
	if(unlikely(skb == NULL))
		return;
	if(likely(atomic_read(&skb->users) == 1))
		smp_rmb();
	else if (likely(!atomic_dec_and_test(&skb->users)))
		return;
	__kfree_skb(skb);
}

static int __must_check naonet_init_rx_queue(struct naonet *nn)
{
	size_t rx_queue_len;
	struct net_device *dev = NULL;

	if(!nn)
		return -EINVAL;

	dev = dev_get_by_name(&init_net, nn->dev_name);
	if(dev == NULL)
		return -ENOENT;
	if(!netif_device_present(dev) || !netif_running(dev))
		return -EIO;

	rx_queue_len = (dev->mtu + sizeof(struct naohdr)) 
			* nn->rx_queue_elems;
	
	nn->rx_queue = kfifo_alloc(rx_queue_len, GFP_KERNEL, 
				   &nn->rx_queue_lock);
	if(IS_ERR(nn->rx_queue)) {
		printk(KERN_ERR "naonet: rx_queue allocation failed\n");
		return -ENOMEM;
	}

	return 0;
}

static ssize_t naonet_push_into_rx_queue(struct naonet *nn, 
					 struct naohdr *head, 
					 char *buff)
{
	unsigned int rc;

	if(!nn || !head || !buff)
		return -EINVAL;
	if(kfifo_avail(nn->rx_queue) < (sizeof(*head) + ntohs(head->len)))
		return -ENOMEM;

	rc = kfifo_put(nn->rx_queue, (unsigned char *) head, sizeof(*head));
	if(rc != sizeof(*head)) {
		rc = -EIO;
		goto err;
	}

	rc = kfifo_put(nn->rx_queue, (unsigned char *) buff, ntohs(head->len));
	if(rc != ntohs(head->len))
		return -EIO; /* XXX undo head */

	wake_up_interruptible(&nn->rx_queue_wait);

	return 0;
err:
	return rc;
}

static ssize_t __must_check naonet_extract_from_rx_queue(struct naonet *nn, 
						__user char *buff, size_t len)
{
	unsigned int rc;
	unsigned count = 0;

	struct naohdr *head = (struct naohdr *) buff;

	if(!nn || !buff || len == 0)
		return -EINVAL;
	if(kfifo_is_empty(nn->rx_queue))
		return 0;
	if(len < sizeof(*head))
		return -ENOMEM;

	rc = kfifo_to_user(nn->rx_queue, head, sizeof(*head), &count);
	if(count != sizeof(*head)) {
		rc = -EIO;
		goto err;
	}
	if(len - sizeof(*head) < ntohs(head->len)) {
		rc = -ENOMEM;
		goto err;
	}

	rc = kfifo_to_user(nn->rx_queue, buff + sizeof(*head), ntohs(head->len), 
			   &count);
	if(count != ntohs(head->len))
		return -EIO;

	return count + sizeof(*head);
err:
	return rc;
}

static void naonet_free_rx_queue(struct naonet *nn)
{
	kfifo_free(nn->rx_queue);
}

static inline int naonet_rx_queue_not_empty(struct naonet *nn)
{
	return (!kfifo_is_empty(nn->rx_queue));
}

int naonet_receive(struct sk_buff *skb, struct net_device *dev, 
		   struct packet_type *pt, struct net_device *orig_dev)
{
	int rc;
	uint32_t len;

	char *payload = NULL;
	unsigned long flags;

	struct naonet *nn = &nn_control;
	struct naohdr *naoh = NULL;

	if(skb->pkt_type == PACKET_OTHERHOST) {
		kfree_skb(skb);
		return 0;
	}

	skb = skb_share_check(skb, GFP_ATOMIC);
	if(!skb)
		goto out;

	rc = pskb_may_pull(skb, sizeof(*naoh));
	if(!rc)
		goto inhdr_error;

	naoh = naonet_fetch_header(skb);

	len = ntohs(naoh->len);
	if(skb->len < len)
		goto drop;
	if(naoh->src_id == nn->src_id)
		goto drop;

	atomic_inc(&nn->rx_frames_bq);

	payload = naonet_fetch_payload(skb);
	rc = naonet_push_into_rx_queue(nn, naoh, payload);
	if(rc < 0)
		printk(KERN_ERR "naonet: element not queued (%d)\n", rc);

	if(nn->rx_hook) {
		read_lock_irqsave(&nn->rx_hook_lock, flags);
		nn->rx_hook(naoh, payload);
		read_unlock_irqrestore(&nn->rx_hook_lock, flags);
	}

	atomic_inc(&nn->rx_frames_aq);

	naonet_consume_skb(skb);
	return NET_RX_SUCCESS;
inhdr_error:
drop:
	atomic_inc(&nn->rx_frames_drop);
	kfree_skb(skb);
out:
	return NET_RX_DROP;
}

void naonet_register_rx_hook(struct naonet *nn, 
			void (*__rx_hook)(struct naohdr *head, char *payload))
{
	unsigned long flags;

	write_lock_irqsave(&nn->rx_hook_lock, flags);
	nn->rx_hook = __rx_hook;
	write_unlock_irqrestore(&nn->rx_hook_lock, flags);
}

void naonet_unregister_rx_hook(struct naonet *nn)
{
	naonet_register_rx_hook(nn, NULL);
}

static int naonet_open(struct inode *inode, struct file *file)
{
	return 0;
}

static int naonet_release(struct inode *inode, struct file *file)
{
	return 0;
}

static ssize_t naonet_push(struct file *file, const char *buff, size_t len, 
			   loff_t *off)
{
	int rc;
	struct naonet *nn = &nn_control;

	if((rc = naonet_send(nn, buff, len)) != 0) {
		printk(KERN_ERR "naonet: naonet_send failed (%d)!\n", rc);
		return -EIO;
	}

	return len;
}

static ssize_t naonet_pull(struct file *file, char *buff, size_t len, 
			   loff_t *off)
{
	size_t nlen, rc = 0, count = 0;
	struct naonet *nn = &nn_control;

	if(len == 0)
		return 0;

	while(len > 0) {
		nlen = len;

		nlen = naonet_extract_from_rx_queue(nn, buff, nlen);
		if(nlen == 0) {
			if (file->f_flags & O_NONBLOCK) {
				rc = -EAGAIN;
				break;
			}

			wait_event_interruptible(nn->rx_queue_wait, 
						 naonet_rx_queue_not_empty(nn));

			if(signal_pending(current)) {
				rc = -ERESTARTSYS;
				break;
			}

			continue;
		}

		if(nlen < 0) {
			rc = nlen;
			break;
		}

		count += nlen;
		buff += nlen;
		len -= nlen;
		break;
	}

	return (count ? count : rc);
}

static int naonet_ioctl(struct inode *inode, struct file *file, 
			unsigned int cmd, unsigned long arg)
{
	return 0;
}

static struct file_operations naonet_fops __read_mostly = {
	.owner   = THIS_MODULE,
	.ioctl   = naonet_ioctl,
	.open    = naonet_open,
	.write   = naonet_push,
	.read    = naonet_pull,
	.release = naonet_release,
};

static struct miscdevice naonet_misc_dev __read_mostly = {
	.fops  = &naonet_fops,
	.minor = MISC_DYNAMIC_MINOR,
	.name  = DEV_NAME,
};

static struct packet_type naonet_packet_type __read_mostly = {
	.type = cpu_to_be16(ETH_P_NAO),
	.func = naonet_receive,
};

static int naonet_procfs_ctl(char *page, char **start, off_t offset, 
			     int count, int *eof, void *data)
{
	unsigned long flags;
	struct naonet *nn = data;

	off_t len = 0;
	read_lock_irqsave(&nn->ctl_lock, flags);
	len += sprintf(page + len, "dev:%s\n", nn->dev_name);
	len += sprintf(page + len, "src:0x%x\n", nn->src_id);
	len += sprintf(page + len, "qle:%u\n", nn->rx_queue_elems);
	read_unlock_irqrestore(&nn->ctl_lock, flags);
	*eof = 1;

	return len;
}

static int naonet_procfs_stat(char *page, char **start, off_t offset, 
			      int count, int *eof, void *data)
{
	struct naonet *nn = data;

	off_t len = 0;
	len += sprintf(page + len, "rx_bq:%df\n", atomic_read(&nn->rx_frames_bq));
	len += sprintf(page + len, "rx_aq:%df\n", atomic_read(&nn->rx_frames_aq));
	len += sprintf(page + len, "rx_drop:%df\n", atomic_read(&nn->rx_frames_drop));
	len += sprintf(page + len, "tx:%df\n", atomic_read(&nn->tx_frames));
	*eof = 1;

	return len;
}

static int naonet_procfs_id(char *page, char **start, off_t offset, 
			    int count, int *eof, void *data)
{
	unsigned long flags;
	struct naonet *nn = data;

	off_t len = 0;
	read_lock_irqsave(&nn->ctl_lock, flags);
	len += sprintf(page + len, "%u\n", nn->src_id);
	read_unlock_irqrestore(&nn->ctl_lock, flags);
	*eof = 1;

	return len;
}

static int naonet_procfs_init(struct naonet *nn)
{
	atomic_set(&nn->rx_frames_bq, 0);
	atomic_set(&nn->rx_frames_aq, 0);
	atomic_set(&nn->rx_frames_drop, 0);
	atomic_set(&nn->tx_frames, 0);

	naonet_dir = proc_mkdir(NAONET_PROC_DIR, NULL);
	if(!naonet_dir)
		goto out_1;
	naonet_dir->owner = THIS_MODULE;

	naonet_inf = create_proc_read_entry(NAONET_PROC_INF, 0444, naonet_dir, 
					    naonet_procfs_id, nn);
	if(!naonet_inf)
		goto out_2;
        naonet_inf->owner = THIS_MODULE;

	naonet_stat = create_proc_read_entry(NAONET_PROC_STAT, 0444, naonet_dir, 
					    naonet_procfs_stat, nn);
	if(!naonet_stat)
		goto out_3;
        naonet_inf->owner = THIS_MODULE;

	naonet_ctl = create_proc_read_entry(NAONET_PROC_CTL, 0444, naonet_dir, 
					    naonet_procfs_ctl, nn);
	if(!naonet_ctl)
		goto out_4;
        naonet_ctl->owner = THIS_MODULE;

        return 0;

out_4:
        remove_proc_entry(NAONET_PROC_STAT, naonet_dir);
out_3:
        remove_proc_entry(NAONET_PROC_INF, naonet_dir);
out_2:
        remove_proc_entry(NAONET_PROC_DIR, NULL);
out_1:
        printk(KERN_ERR "naonet: unable to create procfs dir entry!\n");
        return (-ENOMEM);
}

static void naonet_procfs_cleanup(void)
{
	remove_proc_entry(NAONET_PROC_CTL, naonet_dir);
	remove_proc_entry(NAONET_PROC_STAT, naonet_dir);
	remove_proc_entry(NAONET_PROC_INF, naonet_dir);
	remove_proc_entry(NAONET_PROC_DIR, NULL);
}

int __init naonet_init(void)
{
	int rc = 0;
	char *input = config;

	naonet_init_struct(&nn_control);
	naonet_set_default_options(&nn_control);
	if(strnlen(input, MAX_PARAM_LENGTH)) {
		rc = naonet_parse_options(&nn_control, config);
		if(rc < 0)
			goto err_ret;
	}
	naonet_print_options(&nn_control);

	if(naonet_init_rx_queue(&nn_control) != 0)
		goto err_ret;

	dev_add_pack(&naonet_packet_type);

	if(naonet_procfs_init(&nn_control) != 0)
		goto err_proc;

	if(misc_register(&naonet_misc_dev) != 0)
		goto err;

#ifdef __DEBUG__
	naonet_register_rx_hook(&nn_control, debug_hook);
#endif /* __DEBUG__ */

	printk(KERN_INFO "naonet: proto ETH_P_NAO registered\n");
	return 0;
err:
	naonet_procfs_cleanup();
err_proc:
	dev_remove_pack(&naonet_packet_type);
	naonet_free_rx_queue(&nn_control);
err_ret:
	return rc;
}

void __exit naonet_exit(void)
{
	naonet_procfs_cleanup();
	naonet_unregister_rx_hook(&nn_control);
	misc_deregister(&naonet_misc_dev);
	dev_remove_pack(&naonet_packet_type);
	naonet_free_rx_queue(&nn_control);

	printk(KERN_INFO "naonet: proto ETH_P_NAO unregistered\n");
}

module_init(naonet_init);
module_exit(naonet_exit);

