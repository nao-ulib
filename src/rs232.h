/*
 * nao-ulib
 * Copyright 2011 Daniel Borkmann <dborkma@tik.ee.ethz.ch>
 * Subject to the GPL.
 * Nao-Team HTWK,
 * Faculty of Computer Science, Mathematics and Natural Sciences,
 * Leipzig University of Applied Sciences (HTWK Leipzig)
 */

#ifndef RS232_H
#define RS232_H

extern int open_sport(int number);
extern ssize_t write_sport(int fd, char *out, size_t len);
extern ssize_t read_sport(int fd, char *in, size_t len);
extern void close_sport(int fd);

#endif /* RS232_H */
