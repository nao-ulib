/*
 * nao-ulib
 * Copyright 2011 Daniel Borkmann <dborkma@tik.ee.ethz.ch>
 * Subject to the GPL.
 * Nao-Team HTWK,
 * Faculty of Computer Science, Mathematics and Natural Sciences,
 * Leipzig University of Applied Sciences (HTWK Leipzig)
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <assert.h>
#include <fcntl.h>
#include <getopt.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <asm/types.h>
#include <linux/videodev2.h>

#include "mmap_v4l.h"
#include "comp_x86.h"
#include "xmalloc.h"
#include "die.h"

#define CLIP(color) (unsigned char) (((color) > 0xFF) ?  \
                    0xff : (((color) < 0) ? 0 : (color)))

void convert_v4l_yuyv_to_rgb24(const uint8_t *src, uint8_t *dest,
			       int width, int height)
{
        /* From: Hans de Goede <j.w.r.degoede@hhs.nl> */
        int j;

        while(--height >= 0) {
                for(j = 0; j < width; j += 2) {
                        int u = src[1];
                        int v = src[3];
                        int u1 = (((u - 128) << 7) +  (u - 128)) >> 6;
                        int rg = (((u - 128) << 1) +  (u - 128) +
                                  ((v - 128) << 2) + ((v - 128)  << 1)) >> 3;
                        int v1 = (((v - 128) << 1) +  (v - 128)) >> 1;

                        *dest++ = CLIP(src[0] + v1);
                        *dest++ = CLIP(src[0] - rg);
                        *dest++ = CLIP(src[0] + u1);
                        *dest++ = CLIP(src[2] + v1);
                        *dest++ = CLIP(src[2] - rg);
                        *dest++ = CLIP(src[2] + u1);
                        src += 4;
                }
        }
}

int read_v4l_frame(int fd, unsigned int nbuffs,
		   struct v4l_buff *buffs,
		   int (*rx_v4l_cb)(uint8_t *frame, size_t len))
{
	int ret, rc;
	struct v4l2_buffer buf;

	if (unlikely(!buffs || !nbuffs))
		return -EINVAL;

        memset(&buf, 0, sizeof(buf));
        buf.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_MMAP;

	ret = xioctl(fd, VIDIOC_DQBUF, &buf);
	if (ret < 0) {
		switch(errno) {
		case EAGAIN:
			return 0;
		case EIO:
		default:
			panic("I/O dequeue error!\n");
		}
	}

	if (unlikely(buf.index >= nbuffs))
		panic("I/O overflow error!\n");

	rc = rx_v4l_cb(buffs[buf.index].start, buffs[buf.index].length);

	ret = xioctl(fd, VIDIOC_QBUF, &buf);
	if (ret < 0)
		panic("I/O enqueue error!\n");

	return rc;
}

int open_v4l_device(char *dev)
{
	int fd, ret;
	struct stat st;

	if (!dev)
		dev = "/dev/video0";

	memset(&st, 0, sizeof(st));
	ret = stat(dev, &st);
        if (ret < 0)
		panic("Cannot identify %s!\n", dev);

	if (!S_ISCHR(st.st_mode))
		panic("%s is no device!\n", dev);

	fd = open(dev, O_RDWR | O_NONBLOCK, 0);
	if (fd < 0)
		panic("Cannot open %s!\n", dev);

        return fd;
}

void close_vl4_device(int fd)
{
	if (fd < 0)
		return;
	close(fd);
}

int start_v4l_capturing(int fd, unsigned int nbuffs)
{
	int ret;
	unsigned int i;
	enum v4l2_buf_type type;

	for (i = 0; i < nbuffs; ++i) {
		struct v4l2_buffer buf;

                memset(&buf, 0, sizeof(buf));
                buf.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
                buf.memory = V4L2_MEMORY_MMAP;
                buf.index  = i;

		ret = xioctl(fd, VIDIOC_QBUF, &buf);
                if (ret < 0)
			panic("I/O enqueue error!\n");
        }

	type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	ret = xioctl(fd, VIDIOC_STREAMON, &type);
	if (ret < 0)
		panic("Cannot start capture!\n");

	return 0;
}

int stop_v4l_capturing(int fd)
{
	int ret;
	enum v4l2_buf_type type;

	type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	ret = xioctl(fd, VIDIOC_STREAMOFF, &type);
	if (ret < 0)
		panic("Cannot stop capture!\n");

	return 0;
}

int init_mmap(int fd, const char *dev, unsigned int *nbuffs,
	      struct v4l_buff **buffs, int frames)
{
	int ret;
	struct v4l2_requestbuffers setup;

	if (unlikely(!dev || !nbuffs || !buffs))
		return -EINVAL;
	if (frames == 0)
		frames = 3; /* default */

	memset(&setup, 0, sizeof(setup));
	setup.count  = frames; /* queuelen */
	setup.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	setup.memory = V4L2_MEMORY_MMAP;

	ret = xioctl(fd, VIDIOC_REQBUFS, &setup);
	if (ret < 0) {
		if (EINVAL == errno)
			panic("No mmap support!\n");
		else
			panic("I/O error!\n");
        }

	if (setup.count < 2)
		panic("Insufficient buffer memory!\n");

	(*buffs) = xzmalloc(setup.count * sizeof(**buffs));

	for ((*nbuffs) = 0; (*nbuffs) < setup.count; ++(*nbuffs)) {
		struct v4l2_buffer buf;

		memset(&buf, 0, sizeof(buf));
		buf.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buf.memory = V4L2_MEMORY_MMAP;
		buf.index  = (*nbuffs);

		ret = xioctl(fd, VIDIOC_QUERYBUF, &buf);
		if (ret < 0)
			panic("Cannot query buf!\n");

		(*buffs)[(*nbuffs)].length = buf.length;
		(*buffs)[(*nbuffs)].start = mmap(NULL, buf.length,
						 PROT_READ | PROT_WRITE,
						 MAP_SHARED, fd, buf.m.offset);
		if (MAP_FAILED == (*buffs)[(*nbuffs)].start)
			panic("Error on mmap!\n");
	}
       
	return 0;
}

void set_v4l_ctl_or_die(int fd, unsigned int id, int value)
{
	int ret;
	struct v4l2_control control_s;
	struct v4l2_queryctrl queryctrl;

	memset(&queryctrl, 0, sizeof(queryctrl));
	queryctrl.id = id;

	ret = xioctl(fd, VIDIOC_QUERYCTRL, &queryctrl);
	if (ret < 0)
		panic("Cannot query cmd id %x!\n", id);
	if (queryctrl.flags & V4L2_CTRL_FLAG_DISABLED)
		panic("Cmd id %x disabled!\n", id);
	if (queryctrl.type != V4L2_CTRL_TYPE_BOOLEAN && 
	    queryctrl.type != V4L2_CTRL_TYPE_INTEGER && 
	    queryctrl.type != V4L2_CTRL_TYPE_MENU)
		panic("Cmd id %x has unsupported query type!\n", id);
	if (value < queryctrl.minimum)
		value = queryctrl.minimum;
	if (value > queryctrl.maximum)
		value = queryctrl.maximum;
	if (value < 0)
		value = queryctrl.default_value;

	memset(&control_s, 0, sizeof(control_s));
	control_s.id = id;
	control_s.value = value;

	ret = xioctl(fd, VIDIOC_S_CTRL, &control_s);
	if (ret < 0) 
		panic("Cannot set cmd id %x!\n", id);
}

int init_v4l_device(int fd, int width, int height)
{
	int ret;
	v4l2_std_id esid0;
	struct v4l2_capability cap;
	struct v4l2_control control;
	struct v4l2_cropcap cropcap;
	struct v4l2_crop crop;
	struct v4l2_format fmt;
	struct v4l2_streamparm fps;

	memset(&cap, 0, sizeof(cap));
	ret = xioctl(fd, VIDIOC_QUERYCAP, &cap);
	if (ret < 0) {
		if(EINVAL == errno)
			panic("This is no V4L2 device!\n");
                else
			panic("V4L2 error on device!\n");
	}

	if ((cap.capabilities & V4L2_CAP_VIDEO_CAPTURE) == 0)
		panic("This is no video capture device!\n");

	if ((cap.capabilities & V4L2_CAP_STREAMING) == 0)
		panic("No streaming support for this device!\n");

	memset(&cropcap, 0, sizeof(cropcap));
	cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

	ret = xioctl(fd, VIDIOC_CROPCAP, &cropcap);
	if (ret == 0) {
		memset(&crop, 0, sizeof(crop));
		crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		crop.c    = cropcap.defrect;

		ret = xioctl(fd, VIDIOC_S_CROP, &crop);
		if (ret < 0) {
			switch(errno) {
			case EINVAL:
				/* Cropping not supported. */
			default:
				break;
			}
		}
	}

	/* This is for the buggy AMD driver, we need to do this first! */
	memset(&control, 0, sizeof(control));
	control.id    = V4L2_CID_CAM_INIT;
	control.value = 0;

	ret = xioctl(fd, VIDIOC_S_CTRL, &control);
	if (ret < 0)
		panic("%s cannot set cam init!\n");

	/* Aldebaran setting */
	if (width == 640 && height == 480)
		esid0 = 0x08000000UL; /*VGA*/
	else if (width == 320 && height == 240)
		esid0 = 0x04000000UL; /*QVGA*/
	else
		panic("Panic over invalid width/height settings!\n");

	ret = xioctl(fd, VIDIOC_S_STD, &esid0);
	if (ret < 0)
		panic("Cannot set Aldebaran video i/o std!\n");

	memset(&fmt, 0, sizeof(fmt));
	fmt.type                = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	fmt.fmt.pix.width       = width;
	fmt.fmt.pix.height      = height;
	fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
	fmt.fmt.pix.field       = V4L2_FIELD_INTERLACED;

	ret = xioctl(fd, VIDIOC_S_FMT, &fmt);
	if (ret < 0)
		panic("Cannot set v4l format!\n");

	set_v4l_ctl_or_die(fd, V4L2_CID_AUTO_WHITE_BALANCE, 0);
	set_v4l_ctl_or_die(fd, V4L2_CID_AUTOGAIN, 0);
	set_v4l_ctl_or_die(fd, V4L2_CID_HUE_AUTO, 0);
	set_v4l_ctl_or_die(fd, V4L2_CID_EXPOSURE_AUTO, 0);
	set_v4l_ctl_or_die(fd, V4L2_CID_HFLIP, 0);
	set_v4l_ctl_or_die(fd, V4L2_CID_VFLIP, 0);

	memset(&fps, 0, sizeof(fps));
	fps.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

	ret = xioctl(fd, VIDIOC_G_PARM, &fps);
	if (ret < 0)
		panic("Cannot query fps param!\n");

	fps.parm.capture.timeperframe.numerator   = 1;
	fps.parm.capture.timeperframe.denominator = 30;

	ret = xioctl(fd, VIDIOC_S_PARM, &fps);
	if (ret < 0)
		panic("Cannot set fps param!\n");

	return 0;
}

int cleanup_v4l_device(int fd, unsigned int nbuffs,
		       struct v4l_buff *buffs)
{
	int ret;
	unsigned int i;

	for (i = 0; i < nbuffs; ++i) {
		ret = munmap(buffs[i].start, buffs[i].length);
		if (ret < 0)
			panic("Cannot munmap!\n");
	}

	xfree(buffs);
	return 0;
}

void debug_v4l_struct_buffer(struct v4l2_buffer *img)
{
	printf("index: %u\n", img->index);
	printf("type:  %u\n", img->type);
	printf("bytes used: %u\n", img->bytesused);
	printf("flags: %u (0x%x), mapped:%d, queued:%d, done:%d\n",
		img->flags, img->flags, 
	       (img->flags & V4L2_BUF_FLAG_MAPPED), 
	       (img->flags & V4L2_BUF_FLAG_QUEUED),
	       (img->flags & V4L2_BUF_FLAG_DONE));
	printf("field: %u\n", img->field);
	printf("sequence: %u\n", img->sequence);
	printf("length: %u\n", img->length);
	printf("input: %u\n", img->input);
	printf("reserved: %u\n\n", img->reserved);
}


