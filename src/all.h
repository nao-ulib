/*
 * nao-ulib
 * Copyright 2011 Daniel Borkmann <dborkma@tik.ee.ethz.ch>
 * Subject to the GPL.
 * Nao-Team HTWK,
 * Faculty of Computer Science, Mathematics and Natural Sciences,
 * Leipzig University of Applied Sciences (HTWK Leipzig)
 */

#ifndef NAO_H
#define NAO_H

#ifndef CODE_IS_GPL
#error "Your code is not GPL licensed! You cannot use this lib!"
#endif

#include "atomic_x86.h"
#include "cache_x86.h"
#include "comp_x86.h"
#include "locking_x86.h"
#include "memcpy_x86.h"
#include "ticks_x86.h"
#include "bithacks.h"
#include "mmap_v4l.h"
#include "die.h"
#include "math2.h"
#include "sort.h"
#include "psched.h"
#include "deflate.h"
#include "stacktrace.h"
#include "i2c.h"
#include "rbtree.h"
#include "syslog.h"
#include "mtrand.h"
#include "naonet.h"
#include "notifier.h"
#include "rbtree.h"
#include "psched.h"
#include "rs232.h"
#include "signals.h"
#include "stacktrace.h"
#include "strlcpy.h"
#include "syslog.h"
#include "urcu.h"
#include "write_or_die.h"
#include "xmalloc.h"
/* tlsf is wrapped via xmalloc_rt */
#include "xmalloc_rt.h"

#endif /* NAO_H */
