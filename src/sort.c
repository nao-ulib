/*
 * nao-ulib
 * Copyright (C) 2009 Daniel Borkmann <borkmann@gnumaniacs.org>
 * Copyright (C) 2009 Thomas Reinhardt <treinhar@imn.htwk-leipzig.de>
 * Copyright (C) 2009 Rico Tilgner <rtilgner@imn.htwk-leipzig.de>
 * Subject to the GPL.
 * Nao-Team HTWK,
 * Faculty of Computer Science, Mathematics and Natural Sciences,
 * Leipzig University of Applied Sciences (HTWK Leipzig)
 */

#include <stdio.h>

#include "xmalloc.h"
#include "sort.h"

static void merge(int lo, int m, int hi, float *a, float *b)
{
	int i, j, k;

	i = 0; j = lo;
	while (j <= m)
		b[i++] = a[j++];

	i = 0; k = lo;
	while (k < j && j <= hi)
		if (b[i] <= a[j])
			a[k++] = b[i++];
		else
			a[k++] = a[j++];

	while (k < j)
		a[k++] = b[i++];
}

static inline void mergesort(int lo, int hi, float *a, float *b)
{
	if (lo < hi) {
		int m = (lo + hi) / 2;

		mergesort(lo, m, a, b);
		mergesort(m + 1, hi, a, b);
		merge(lo, m, hi, a, b);
	}
}

void sort(float *a, size_t n)
{
	float *b = xmalloc(n * sizeof(float));
	mergesort(0, n - 1, a, b);
	xfree(b);
}
