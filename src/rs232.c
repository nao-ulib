/*
 * nao-ulib
 * Copyright 2011 Daniel Borkmann <dborkma@tik.ee.ethz.ch>
 * Subject to the GPL.
 * Nao-Team HTWK,
 * Faculty of Computer Science, Mathematics and Natural Sciences,
 * Leipzig University of Applied Sciences (HTWK Leipzig)
 */

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>

#include "rs232.h"
#include "die.h"

int open_sport(int number)
{
	int fd;
	char port_name[64];
        struct termios term;

	if (number < 0)
		return -EINVAL;

	memset(port_name, 0, sizeof(port_name));
	sprintf(port_name, "/dev/ttyS%d", number);
	info("Opening port %s!\n", port_name);

	fd = open(port_name, O_RDWR | O_NOCTTY | O_NDELAY);
	if (fd < 0)
		panic("Cannot open serial port!\n");

	tcgetattr(fd, &term);
	tcflush(fd, TCIFLUSH);

	term.c_cflag = B9600 | CS8 |CREAD | CLOCAL | HUPCL;

	cfsetospeed(&term, B9600);
	tcsetattr(fd, TCSANOW, &term);

	info("cflag=%08x\n", term.c_cflag);
	info("oflag=%08x\n", term.c_oflag);
	info("iflag=%08x\n", term.c_iflag);
	info("lflag=%08x\n", term.c_lflag);
	info("line=%02x\n", term.c_line);

	return fd;
}

ssize_t write_sport(int fd, char *out, size_t len)
{
	return write(fd, out, len);
}

ssize_t read_sport(int fd, char *in, size_t len)
{
	return read(fd, in, len);
}

void close_sport(int fd)
{
	close(fd);
}
