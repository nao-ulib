/*
 * nao-ulib
 * Copyright 2011 Daniel Borkmann <dborkma@tik.ee.ethz.ch>
 * Subject to the GPL.
 * Nao-Team HTWK,
 * Faculty of Computer Science, Mathematics and Natural Sciences,
 * Leipzig University of Applied Sciences (HTWK Leipzig)
 */

#include <stdint.h>
#include <errno.h>

#include "notifier.h"
#include "comp_x86.h"

/*
 * Event notification framework based on the idea of the Linux kernel
 * notification chains by Alan Cox which have originally been written 
 * for event delivery within the networking subsystem.
 */

int register_event_hook(struct event_block **head,
		        struct event_block *block)
{
	if (!block || !head)
		return -EINVAL;
	if (!block->hook)
		return -EINVAL;

	while ((*head) != NULL) {
		if (block->prio > (*head)->prio)
			break;

		head = &((*head)->next);
	}

	block->next = (*head);
	(*head) = block;
	barrier();

	return 0;
}

int register_event_hook_once(struct event_block **head,
			     struct event_block *block)
{
	if (!block || !head)
		return -EINVAL;
	if (!block->hook)
		return -EINVAL;

	while ((*head) != NULL) {
		if (unlikely(block == (*head)))
			return -EEXIST;

		if (block->prio > (*head)->prio)
			break;

		head = &((*head)->next);
	}

	block->next = (*head);
	(*head) = block;
	barrier();

	return 0;
}

int unregister_event_hook(struct event_block **head,
			  struct event_block *block)
{
	if (!block || !head)
		return -EINVAL;

	while ((*head) != NULL) {
		if (unlikely(block == (*head))) {
			(*head) = block->next;
			barrier();
			break;
		}

		head = &((*head)->next);
	}

	return 0;
}

int call_event_hooks(struct event_block **head, unsigned long event,
		     const void *arg, int *called)
{
	int ret = BLOCK_SUCC_DONE;
	struct event_block *block = *head, *next_block;

	if (!head || !arg)
		return -EINVAL;
	if (called)
		(*called) = 0;

	while (block) {
		next_block = block->next;

		ret = block->hook(block, event, arg);
		if (ret & BLOCK_STOP_CHAIN)
			break;

		if(called)
			(*called)++;

		block = next_block;
	}

	return ret;
}

