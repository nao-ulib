/*
 * nao-ulib
 * Copyright 2011 Daniel Borkmann <dborkma@tik.ee.ethz.ch>
 * Subject to the GPL.
 * Nao-Team HTWK,
 * Faculty of Computer Science, Mathematics and Natural Sciences,
 * Leipzig University of Applied Sciences (HTWK Leipzig)
 */

#ifndef TICKS_X86_H
#define TICKS_X86_H

/*
 * Generate 64-bit timestamp.
 */

static unsigned long long getticks(void)
{
	unsigned int __a,__d;

	__asm__ __volatile__("rdtsc" : "=a" (__a), "=d" (__d));
	return ((long long)__a) | (((long long)__d)<<32);
}

#endif /* TICKS_X86_H */
