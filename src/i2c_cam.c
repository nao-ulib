/*
 * nao-ulib
 * Copyright 2011 Daniel Borkmann <dborkma@tik.ee.ethz.ch>
 * Subject to the GPL.
 * Nao-Team HTWK,
 * Faculty of Computer Science, Mathematics and Natural Sciences,
 * Leipzig University of Applied Sciences (HTWK Leipzig)
 */

/* i2c cam opcodes from B-Human */

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "mmap_v4l.h"
#include "xmalloc.h"
#include "die.h"
#include "i2c.h"

int i2c_select_lower_cam(void)
{
	int fd, ret;
	char c;
	unsigned char cmd[2] = {2, 0};

	/* quickhack */
	fd = open("/dev/i2c-0", O_RDWR);
	if (fd < 0)
		panic("Cannot open i2c dev!\n");

	ret = xioctl(fd, 0x703, (void *) 8);
	if (ret != 0)
		panic("ioctl 0x703 on i2c-0!\n");

	c = i2c_smbus_read_byte_data(fd, 170);
	if (c < 2)
		panic("i2c bus, read 0x%x!\n", c);

	ret = i2c_smbus_write_block_data(fd, 220, 1, cmd);
	if (ret == -1)
		panic("i2c bus read error!\n");

	close(fd);
	printf("Lower Nao cam selected!\n");
	return 0;
}

int i2c_select_upper_cam(void)
{
	int fd, ret;
	char c;
	unsigned char cmd[2] = {1, 0};

	/* quickhack */
	fd = open("/dev/i2c-0", O_RDWR);
	if (fd < 0)
		panic("Cannot open i2c dev!\n");

	ret = xioctl(fd, 0x703, (void *) 8);
	if (ret != 0)
		panic("ioctl 0x703 on i2c-0!\n");

	c = i2c_smbus_read_byte_data(fd, 170);
	if (c < 2)
		panic("i2c bus, read 0x%x!\n", c);

	ret = i2c_smbus_write_block_data(fd, 220, 1, cmd);
	if (ret == -1)
		panic("i2c bus read error!\n");

	close(fd);
	printf("Upper Nao cam selected.\n");
	return 0;
}

