/*
 * nao-ulib
 * Copyright 2010 - 2011 Daniel Borkmann <dborkma@tik.ee.ethz.ch>
 * Subject to the GPL.
 * Nao-Team HTWK,
 * Faculty of Computer Science, Mathematics and Natural Sciences,
 * Leipzig University of Applied Sciences (HTWK Leipzig)
 */

#ifndef NAONET_H
#define NAONET_H

#include <stdint.h>
#include <pthread.h>

/* Message type codes */
#define NAOMSG_TYPE_ALIVE  0
#define NAOMSG_TYPE_WORLD  1
#define NAOMSG_TYPE_CMD    2
/* ... */

/* rx_hook return codes */
#define BLOCK_SUCC_DONE    0
#define BLOCK_STOP_CHAIN   1

/* Priority for naonet_block */
typedef enum {
	PRIO_VERYLOW,
	PRIO_LOW,
	PRIO_MEDIUM,
	PRIO_HIGH,
	PRIO_EXTRA,
} naonet_rx_prio_t;

/* Network header */
typedef struct {
	uint16_t len;
	uint32_t src_id;
	uint16_t opt;
} naonet_header_t;

/* Userspace callback structure */
typedef struct naonet_block {
	uint8_t type;
	int (*rx_hook)(const struct naonet_block *block, uint32_t src, 
		       const char *message, size_t len);
	struct naonet_block *next;
	naonet_rx_prio_t priority;
} naonet_block_t;

extern uint32_t naonet_fetch_id(void);
extern int naonet_init(pthread_t *rx_thread_p);
extern int send_message(char *message, size_t len, uint8_t type);
extern int register_rx_hook(naonet_block_t *block);
extern int register_rx_hook_once(naonet_block_t *block);
extern int unregister_rx_hook(naonet_block_t *block);

#endif /* NAONET_H */
