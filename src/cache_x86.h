/*
 * nao-ulib
 * Copyright 2011 Daniel Borkmann <dborkma@tik.ee.ethz.ch>
 * Subject to the GPL.
 * Nao-Team HTWK,
 * Faculty of Computer Science, Mathematics and Natural Sciences,
 * Leipzig University of Applied Sciences (HTWK Leipzig)
 *
 * Mostly compiler and architecture specific stuff.
 *
 * Some code derived from arch-i386.h
 * Copyright (c) 2006 Paul E. McKenney, IBM.
 * Much code taken from the Linux kernel.  For such code, the option
 * to redistribute under later versions of GPL might not be available.
 */

#ifndef CACHE_X86_H
#define CACHE_X86_H

#define GEODE_CACHE_LINE_SIZE  32
#define ____cacheline_internodealigned_in_smp \
	__attribute__((__aligned__(1 << 6)))

#endif /* CACHE_X86_H */
