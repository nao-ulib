/*
 * nao-ulib
 * Copyright 2011 Daniel Borkmann <dborkma@tik.ee.ethz.ch>
 * Subject to the GPL.
 * Nao-Team HTWK,
 * Faculty of Computer Science, Mathematics and Natural Sciences,
 * Leipzig University of Applied Sciences (HTWK Leipzig)
 */

#ifndef NAO_WRAPPER_H
#define NAO_WRAPPER_H

#ifndef CODE_IS_GPL
#error "Your code is not GPL licensed! You cannot use this lib!"
#endif

#include "../all.h"

#endif /* NAO_WRAPPER_H */
