/*
 * nao-ulib
 * Copyright 2011 Daniel Borkmann <dborkma@tik.ee.ethz.ch>
 * Subject to the GPL.
 * Nao-Team HTWK,
 * Faculty of Computer Science, Mathematics and Natural Sciences,
 * Leipzig University of Applied Sciences (HTWK Leipzig)
 */

#ifndef PSCHED_H
#define PSCHED_H

#define _GNU_SOURCE
#include <unistd.h>
#include <sched.h>
#include <sys/resource.h>

extern void run_on_cpu(int cpu);
extern int set_proc_prio(int priority);
extern int set_sched_status(int policy, int priority);

static inline int get_default_sched_policy(void)
{
	return SCHED_FIFO;
}

static inline int get_default_sched_prio(void)
{
	return sched_get_priority_max(get_default_sched_policy());
}

static inline int get_number_cpus(void)
{
	return sysconf(_SC_NPROCESSORS_CONF);
}

static inline int get_number_cpus_online(void)
{
	return sysconf(_SC_NPROCESSORS_ONLN);
}

#endif /* PSCHED_H */
