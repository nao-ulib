/*
 * nao-ulib
 * Copyright 2011 Daniel Borkmann <dborkma@tik.ee.ethz.ch>
 * Subject to the GPL.
 * Nao-Team HTWK,
 * Faculty of Computer Science, Mathematics and Natural Sciences,
 * Leipzig University of Applied Sciences (HTWK Leipzig)
 */

#ifndef NAONET_H
#define NAONET_H

#include <stdint.h>
#include <stdio.h>
#include <asm/types.h>
#include <linux/videodev2.h>

#ifndef V4L2_CID_AUTOEXPOSURE
# define V4L2_CID_AUTOEXPOSURE              (V4L2_CID_BASE + 32)
#endif
#ifndef V4L2_CID_CAM_INIT
# define V4L2_CID_CAM_INIT                  (V4L2_CID_BASE + 33)
#endif
#ifndef V4L2_CID_AUDIO_MUTE
# define V4L2_CID_AUDIO_MUTE                (V4L2_CID_BASE + 9)
#endif
#ifndef V4L2_CID_POWER_LINE_FREQUENCY
# define V4L2_CID_POWER_LINE_FREQUENCY      (V4L2_CID_BASE + 24)
enum v4l2_power_line_frequency {
	V4L2_CID_POWER_LINE_FREQUENCY_DISABLED = 0,
	V4L2_CID_POWER_LINE_FREQUENCY_50HZ     = 1,
	V4L2_CID_POWER_LINE_FREQUENCY_60HZ     = 2,
};
#endif
#ifndef V4L2_CID_HUE_AUTO
# define V4L2_CID_HUE_AUTO                  (V4L2_CID_BASE + 25)
#endif
#ifndef V4L2_CID_WHITE_BALANCE_TEMPERATURE
# define V4L2_CID_WHITE_BALANCE_TEMPERATURE (V4L2_CID_BASE + 26)
#endif
#ifndef V4L2_CID_SHARPNESS
# define V4L2_CID_SHARPNESS                 (V4L2_CID_BASE + 27)
#endif
#ifndef V4L2_CID_BACKLIGHT_COMPENSATION
# define V4L2_CID_BACKLIGHT_COMPENSATION    (V4L2_CID_BASE + 28)
#endif
#ifndef V4L2_CID_CAMERA_CLASS_BASE
# define V4L2_CID_CAMERA_CLASS_BASE         (V4L2_CTRL_CLASS_CAMERA | 0x900)
#endif
#ifndef V4L2_CID_CAMERA_CLASS
# define V4L2_CID_CAMERA_CLASS              (V4L2_CTRL_CLASS_CAMERA | 1)
#endif
#ifndef V4L2_CID_EXPOSURE_AUTO
# define V4L2_CID_EXPOSURE_AUTO             (V4L2_CID_CAMERA_CLASS_BASE + 1)
enum  v4l2_exposure_auto_type {
	V4L2_EXPOSURE_MANUAL            = 0,
	V4L2_EXPOSURE_AUTO              = 1,
	V4L2_EXPOSURE_SHUTTER_PRIORITY  = 2,
	V4L2_EXPOSURE_APERTURE_PRIORITY = 3
};
#endif
#ifndef V4L2_CID_EXPOSURE_ABSOLUTE
# define V4L2_CID_EXPOSURE_ABSOLUTE         (V4L2_CID_CAMERA_CLASS_BASE + 2)
#endif
#ifndef V4L2_CID_EXPOSURE_AUTO_PRIORITY
# define V4L2_CID_EXPOSURE_AUTO_PRIORITY    (V4L2_CID_CAMERA_CLASS_BASE + 3)
#endif
#ifndef V4L2_CID_PAN_RELATIVE
# define V4L2_CID_PAN_RELATIVE              (V4L2_CID_CAMERA_CLASS_BASE + 4)
#endif
#ifndef V4L2_CID_TILT_RELATIVE
# define V4L2_CID_TILT_RELATIVE             (V4L2_CID_CAMERA_CLASS_BASE + 5)
#endif
#ifndef V4L2_CID_PAN_RESET
# define V4L2_CID_PAN_RESET                 (V4L2_CID_CAMERA_CLASS_BASE + 6)
#endif
#ifndef V4L2_CID_TILT_RESET
# define V4L2_CID_TILT_RESET                (V4L2_CID_CAMERA_CLASS_BASE + 7)
#endif
#ifndef V4L2_CID_PAN_ABSOLUTE
# define V4L2_CID_PAN_ABSOLUTE              (V4L2_CID_CAMERA_CLASS_BASE + 8)
#endif
#ifndef V4L2_CID_TILT_ABSOLUTE
# define V4L2_CID_TILT_ABSOLUTE             (V4L2_CID_CAMERA_CLASS_BASE + 9)
#endif
#ifndef V4L2_CID_FOCUS_ABSOLUTE
# define V4L2_CID_FOCUS_ABSOLUTE            (V4L2_CID_CAMERA_CLASS_BASE + 10)
#endif
#ifndef V4L2_CID_FOCUS_RELATIVE
# define V4L2_CID_FOCUS_RELATIVE            (V4L2_CID_CAMERA_CLASS_BASE + 11)
#endif
#ifndef V4L2_CID_FOCUS_AUTO
# define V4L2_CID_FOCUS_AUTO                (V4L2_CID_CAMERA_CLASS_BASE + 12)
#endif

struct v4l_buff {
	void *start;
	size_t length;
};

/*
 * 0) select cam
 * 1) open_v4l_device
 * 2) init_v4l_device
 * 3) init_mmap
 * 4) start_v4l_capturing
 * 5) while ... read_v4l_frame
 * 6) stop_v4l_capturing
 * 7) cleanup_v4l_device
 * 8) close_vl4_device
 */

extern void convert_v4l_yuyv_to_rgb24(const uint8_t *src,
				      uint8_t *dest, int width, int height);
extern int read_v4l_frame(int fd, unsigned int nbuffs,
			  struct v4l_buff *buffs,
			  int (*rx_v4l_cb)(uint8_t *frame, size_t len));
extern int open_v4l_device(char *dev);
extern void close_vl4_device(int fd);
extern int start_v4l_capturing(int fd, unsigned int nbuffs);
extern int stop_v4l_capturing(int fd);
extern int init_mmap(int fd, const char *dev, unsigned int *nbuffs,
		     struct v4l_buff **buffs, int frames);
extern int cleanup_v4l_device(int fd, unsigned int nbuffs,
			      struct v4l_buff *buffs);
extern void set_v4l_ctl_or_die(int fd, unsigned int id, int value);
extern int init_v4l_device(int fd, int width, int height);
extern void debug_v4l_struct_buffer(struct v4l2_buffer *img);
extern int i2c_select_lower_cam(void);
extern int i2c_select_upper_cam(void);

#endif /* NAONET_H */
