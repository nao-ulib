/*
 * nao-ulib
 * Copyright 2011 Daniel Borkmann <dborkma@tik.ee.ethz.ch>
 * Subject to the GPL.
 * Nao-Team HTWK,
 * Faculty of Computer Science, Mathematics and Natural Sciences,
 * Leipzig University of Applied Sciences (HTWK Leipzig)
 */

#include <stdio.h>
#include <unistd.h>
#include <syslog.h>

static inline void open_syslog(void)
{
	openlog("Nao", LOG_PID | LOG_CONS | LOG_NDELAY, LOG_USER);
}

#define loginf(...)   syslog(LOG_INFO, __VA_ARGS__)
#define logdbg(...)   syslog(LOG_DEBUG, __VA_ARGS__)
#define logwarn(...)  syslog(LOG_WARNING, __VA_ARGS__)
#define logerr(...)   syslog(LOG_ERR, __VA_ARGS__)
#define logcrit(...)  syslog(LOG_CRIT, __VA_ARGS__)

static inline void close_syslog(void)
{
	closelog();
}

#define lognow(prio, ...)                     \
	do {                                  \
		open_syslog();                \
		syslog((prio), __VA_ARGS__);  \
		close_syslog();               \
	} while (0)

