/*
 * nao-ulib
 * Copyright 2011 Daniel Borkmann <dborkma@tik.ee.ethz.ch>
 * Copyright 2011 Tobias Kalbitz <kalbitz@gnumaniacs.org>
 * Subject to the GPL.
 * Nao-Team HTWK,
 * Faculty of Computer Science, Mathematics and Natural Sciences,
 * Leipzig University of Applied Sciences (HTWK Leipzig)
 */

#ifndef MEMCPY_X86_H
#define MEMCPY_X86_H

extern void *geode_exp_mmx_memcpy(void *dest, const void *src, size_t len);

#endif /* MEMCPY_X86_H */
