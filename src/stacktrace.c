/*
 * nao-ulib
 * Copyright 2011 Daniel Borkmann <dborkma@tik.ee.ethz.ch>
 * Subject to the GPL.
 * Nao-Team HTWK,
 * Faculty of Computer Science, Mathematics and Natural Sciences,
 * Leipzig University of Applied Sciences (HTWK Leipzig)
 */

#define _GNU_SOURCE
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <limits.h>
#include <sys/types.h>

#define SYMNAMSIZ        512
#define TMPBUFSIZ        512
#define ADDRESSLISTSIZ    20

#include "comp_x86.h"
#include "strlcpy.h"
#include "die.h"
#include "stacktrace.h"

struct faddress {
	unsigned long real_addr;
	unsigned long closest_addr;
	char name[SYMNAMSIZ];
	char type;
};

static void kill_pipe(int fd, int pid)
{
	close(fd);
	kill(pid, SIGTERM);
}

static int spawn_pipe(const char *cmd, pid_t *pid)
{
	int ret, pipefd[2];

	ret = pipe(pipefd);
	if (ret < 0)
		return ret;

	*pid = fork();
	switch (*pid) {
	case -1:
		close(pipefd[0]);
		close(pipefd[1]);
		ret = -EIO;
		break;
	case 0:
		close(pipefd[0]);
		close(STDOUT_FILENO);
		close(STDERR_FILENO);

		dup2(pipefd[1], STDOUT_FILENO);
		dup2(pipefd[1], STDERR_FILENO);

		/*
		 * The System() call assumes that /bin/sh is
		 * always available, and so will we.
		 */
		execl("/bin/sh", "/bin/sh", "-c", cmd, NULL);
		_die();
		break;
	default:
		close(pipefd[1]);
		ret = pipefd[0];
		break;
	}

	return ret;
}

static int pull_from_pipe(int fd, char *buffer, int max)
{
	char c;
	int i = 0;

	do {
		if (read(fd, &c, 1) < 1)
			return 0;
		if (i < max)
			buffer[i++] = c;
	} while (c != '\n');
	buffer[i] = 0;

	return i;
}

void stacktrace(void)
{
	void *p;
	int i, fd, ret;
	char buffer[TMPBUFSIZ], type;
	char name[TMPBUFSIZ];
	struct faddress syms[ADDRESSLISTSIZ + 1];
	unsigned long addr, hi_addr, lo_addr;
	pid_t pid = 0;

	for (i = 0, p = &p; p; ++i) {
		/*
		 * This is based on code by Steve Coleman
		 * <steve.colemanjhuapl.edu> __builtin_return_address()
		 * only accepts a constant as argument.
		 */
		switch (i) {
		case 0:
			if (__builtin_frame_address(0))
				p = __builtin_return_address(0);
			else	p = NULL;
			break;
		case 1:
			if (__builtin_frame_address(1))
				p = __builtin_return_address(1);
			else	p = NULL;
			break;
		case 2:
			if (__builtin_frame_address(2))
				p = __builtin_return_address(2);
			else	p = NULL;
			break;
		case 3:
			if (__builtin_frame_address(3))
				p = __builtin_return_address(3);
			else	p = NULL;
			break;
		case 4:
			if (__builtin_frame_address(4))
				p = __builtin_return_address(4);
			else	p = NULL;
			break;
		case 5:
			if (__builtin_frame_address(5))
				p = __builtin_return_address(5);
			else	p = NULL;
			break;
		case 6:
			if (__builtin_frame_address(6))
				p = __builtin_return_address(6);
			else	p = NULL;
			break;
		case 7:
			if (__builtin_frame_address(7))
				p = __builtin_return_address(7);
			else	p = NULL;
			break;
		case 8:
			if (__builtin_frame_address(8))
				p = __builtin_return_address(8);
			else	p = NULL;
			break;
		case 9:
			if (__builtin_frame_address(9))
				p = __builtin_return_address(9);
			else	p = NULL;
			break;
		case 10:
			if (__builtin_frame_address(10))
				p = __builtin_return_address(10);
			else	p = NULL;
			break;
		case 11:
			if (__builtin_frame_address(11))
				p = __builtin_return_address(11);
			else	p = NULL;
			break;
		case 12:
			if (__builtin_frame_address(12))
				p = __builtin_return_address(12);
			else	p = NULL;
			break;
		case 13:
			if (__builtin_frame_address(13))
				p = __builtin_return_address(13);
			else	p = NULL;
			break;
		case 14:
			if (__builtin_frame_address(14))
				p = __builtin_return_address(14);
			else	p = NULL;
			break;
		case 15:
			if (__builtin_frame_address(15))
				p = __builtin_return_address(15);
			else	p = NULL;
			break;
		case 16:
			if (__builtin_frame_address(16))
				p = __builtin_return_address(16);
			else	p = NULL;
			break;
		case 17:
			if (__builtin_frame_address(17))
				p = __builtin_return_address(17);
			else	p = NULL;
			break;
		case 18:
			if (__builtin_frame_address(18))
				p = __builtin_return_address(18);
			else	p = NULL;
			break;
		case 19:
			if (__builtin_frame_address(19))
				p = __builtin_return_address(19);
			else	p = NULL;
			break;
		default:
			p = NULL;
			break;
		}

		if (p && i < ADDRESSLISTSIZ) {
			syms[i].real_addr = (unsigned long) p;
			syms[i].closest_addr = 0;
			syms[i].name[0] = 0;
			syms[i].type = ' ';
		} else {
			syms[i].real_addr = 0;
			break;
		}
	}

	strcpy(buffer, "nm -B ");
	strcat(buffer, TARGETNAME);

	lo_addr = ULONG_MAX;
	hi_addr = 0;

	fd = spawn_pipe(buffer, &pid);
	if (fd < 0)
		panic("Cannot spawn pipe to shell!\n");

	while (pull_from_pipe(fd, buffer, sizeof(buffer))) {
		if (buffer[0] == '\n')
			continue;
		ret = sscanf(buffer, "%lx %c %s", &addr, &type, name);
		if (ret != 3)
			continue;
		if (type != 't' && type != 'T')
			continue;
		if (addr == 0)
			continue;
		if (addr < lo_addr)
			lo_addr = addr;
		if (addr > hi_addr)
			hi_addr = addr;
		for (i = 0; syms[i].real_addr != 0; ++i) {
			if (addr <= syms[i].real_addr &&
			    addr > syms[i].closest_addr) {
				syms[i].closest_addr = addr;
				strlcpy(syms[i].name, name, SYMNAMSIZ);
				syms[i].type = type;
			}
		}
	}

	kill_pipe(fd, pid);

	for (i = 0; syms[i].real_addr != 0; ++i) {
		if (syms[i].name[0] == 0 ||
		    syms[i].real_addr <= lo_addr ||
		    syms[i].real_addr >= hi_addr)
			sprintf(buffer, "[%d] 0x%08lx ???\n",
				i, syms[i].real_addr);
		else
			sprintf(buffer, "[%d] 0x%08lx <%s+0x%lx> %c\n",
				i, syms[i].real_addr, syms[i].name,
				syms[i].real_addr - syms[i].closest_addr,
				syms[i].type);
		info(buffer);
	}
}

