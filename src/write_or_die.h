/*
 * nao-ulib
 * Copyright 2010 Daniel Borkmann <dborkma@tik.ee.ethz.ch>
 * Subject to the GPL.
 * Nao-Team HTWK,
 * Faculty of Computer Science, Mathematics and Natural Sciences,
 * Leipzig University of Applied Sciences (HTWK Leipzig)
 */

#ifndef WRITE_OR_DIE_H
#define WRITE_OR_DIE_H

extern void fsync_or_die(int fd, const char *msg);
extern int open_or_die(const char *file, int flags);
extern ssize_t read_or_die(int fd, void *buf, size_t count);
extern ssize_t write_or_die(int fd, const void *buf, size_t count);
extern ssize_t write_or_whine_pipe(int fd, const void *buf, size_t len,
				   const char *msg);
extern ssize_t write_or_whine(int fd, const void *buf, size_t len,
			      const char *msg);

#endif /* WRITE_OR_DIE_H */
