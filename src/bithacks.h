/*
 * nao-ulib
 * Copyright (C) 2009 Daniel Borkmann <borkmann@gnumaniacs.org>
 * Copyright (C) 2009 Thomas Reinhardt <treinhar@imn.htwk-leipzig.de>
 * Copyright (C) 2009 Rico Tilgner <rtilgner@imn.htwk-leipzig.de>
 * Subject to the GPL.
 * Nao-Team HTWK,
 * Faculty of Computer Science, Mathematics and Natural Sciences,
 * Leipzig University of Applied Sciences (HTWK Leipzig)
 */

/*
 * Some stuff is from the Hacker's Delight book, some from
 * http://graphics.stanford.edu/~seander/bithacks.html, some
 * from Quake 3 Arena and some from ourselfes.
 */

#ifndef BITHACKS_H
#define BITHACKS_H

#include <stdbool.h>

/* Inverse square root for floats */
static inline float finvsqrt(float x)
{
	int bf;
	float x_half;

	x_half = .5f * x;
	bf = *(int *) &x;
	bf = 0x5F3759DF - (bf >> 1);
	x = *(float *) &bf;
	x = x * (1.5f - x_half * x * x);

	return x;
}

/* Branch-free absolute function for floats */
static inline float fabs2(float x)
{
	int bf;

	bf = *(int *) &x;
	bf = bf & 0x7FFFFFFF;
	x = *(float *) &bf;

	return x;
}

/* Branch-free compare function for two integers */
static inline int cmp(int x, int y)
{
	return (x > y) - (x < y);
}

/* Check if two integers have opposite signs */
static inline int opp_sign(int x, int y)
{
	return ((x ^ y) < 0);
}

/* Returns sign of interger */
static inline int sign(int x)
{
	return (+1 | (x >> (sizeof(int) * 8 - 1)));
}

/* Return absolute value of integer */
static inline unsigned int iabs(int x)
{
	int const mask = x >> sizeof(int) * 8 - 1;
	return (x + mask) ^ mask;
}

/* Return minimum of x,y */
static inline int min(int x, int y)
{
	return y ^ ((x ^ y) & -(x < y));
}

/* Return maximum of x,y */
static inline int max(int x, int y)
{
	return x ^ ((x ^ y) & -(x < y));
}

/* Determine if x is power of 2 */
static inline int ispow2(unsigned int x)
{
	return x && !(x & (x - 1));
}

/* if (cond) reg |= mask; else reg &= ~mask; */
static inline unsigned int cond_set_bit(bool cond, unsigned int mask,
					unsigned int *reg)
{
	(*reg) ^= (-cond ^ (*reg)) & mask;
}

/* if (cond) reg |= mask; else reg &= ~mask; */
static inline unsigned int cond_set_bit2(int cond, unsigned int mask,
					 unsigned int *reg)
{
	(*reg) ^= (-(!!(cond)) ^ (*reg)) & mask;
}

/* Negate is negate is true */
static inline int cond_negate(bool negate, int x)
{
	return (x ^ -negate) + negate;
}

/* Count Bits in x */
static inline unsigned int count_bits_set(unsigned int x)
{
	unsigned int c = 0;

	for (c = 0; v; c++) {
		v &= v - 1;
	}

	return c;
}

/* Square root calculus for integers */
static inline int sqrt(unsigned x)
{
	unsigned m, b, y;

	m = 0x40000000;
	y = 0;

	while (m != 0) {
		b = y | m;
		y = y >> 1;

		if (x >= b) {
			x -= b;
			y |= m;
		}
		m = m >> 2;
	}

	return y;
}

/* Cube root calculus for integers */
static inline int cbrt(unsigned int x)
{
	int s;
	unsigned b, y;

	s = 30;
	y = 0;

	while (s >= 0) {
		y = y << 1;
		b = (3 * y * (y + 1) + 1) << s;
		s -= 3;

		if (x >= b) {
			x -= b;
			y++;
		}
	}

	return y;
}

/* Exponentiation function for integers */
static inline int exp(int x, unsigned n)
{
	int p, y;

	y = 1;
	p = x;

	while (1) {
		if (n & 1)
			y *= p;
		n = n >> 1;
		if (n == 0)
			return y;
		p *= p;
	}
}

#endif /* BITHACKS_H */
