/*
 * nao-ulib
 * Copyright 2011 Daniel Borkmann <dborkma@tik.ee.ethz.ch>
 * Subject to the GPL.
 * Nao-Team HTWK,
 * Faculty of Computer Science, Mathematics and Natural Sciences,
 * Leipzig University of Applied Sciences (HTWK Leipzig)
 */

#ifndef XMALLOC_H
#define XMALLOC_H

/* TODO: Define wrapper macros for *_no_check and *_check mallocs!  */

#include "comp_x86.h"

extern __hidden void *xmalloc(size_t size);
extern __hidden void *xvalloc(size_t size);
extern __hidden void *xzmalloc(size_t size);
extern __hidden void *xmallocz(size_t size);
extern __hidden void *xmalloc_aligned(size_t size, size_t alignment);
extern __hidden void *xmalloc_geode_l1_cl_aligned(size_t size);
extern __hidden void *xmalloc_geode_l2_cl_aligned(size_t size);
extern __hidden void *xmemdupz(const void *data, size_t len);
extern __hidden void *xcalloc(size_t nmemb, size_t size);
extern __hidden void *xrealloc(void *ptr, size_t nmemb, size_t size);
extern __hidden void xfree(void *ptr);
extern __hidden char *xstrdup(const char *str);
extern __hidden char *xstrndup(const char *str, size_t size);
extern __hidden int xdup(int fd);
extern __hidden void muntrace_handler(int signal);
extern __hidden int xmem_used(void);
extern __hidden int xmem_free(void);
extern __hidden int xmem_totalarena(void);

extern int __xioctl(int fd, int req, void *arg);

static inline int xioctl(int fd, int req, void *arg)
{
	return __xioctl(fd, req, arg);
}


#endif /* XMALLOC_H */
