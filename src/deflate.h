/*
 * nao-ulib
 * Copyright 2009 Daniel Borkmann <dborkma@tik.ee.ethz.ch>
 * Subject to the GPL.
 * Nao-Team HTWK,
 * Faculty of Computer Science, Mathematics and Natural Sciences,
 * Leipzig University of Applied Sciences (HTWK Leipzig)
 */

#ifndef DEFLATE_H
#define DEFLATE_H

extern int z_alloc_or_maybe_die(int z_level);
extern ssize_t z_deflate(char *src, size_t size, char **dst);
extern ssize_t z_inflate(char *src, size_t size, char **dst);
extern void z_free(void);
extern char *z_get_version(void);

#endif /* DEFLATE_H */
