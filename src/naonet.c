/*
 * nao-ulib
 * Copyright 2010 - 2011 Daniel Borkmann <dborkma@tik.ee.ethz.ch>
 * Subject to the GPL.
 * Nao-Team HTWK,
 * Faculty of Computer Science, Mathematics and Natural Sciences,
 * Leipzig University of Applied Sciences (HTWK Leipzig)
 */

/* Needs here & there some cleanups and rewrites */

#include <stdint.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include <netinet/in.h>

#include "naonet.h"
#include "comp_x86.h"

#define NET_DEV_FILE	     "/dev/nao"
#define NET_DEV_MAX_SEG_SIZE 1500

pthread_mutex_t        list_head_lock;
static naonet_block_t *list_head = NULL;

static int __register_rx_hook(naonet_block_t **head, naonet_block_t *block)
{
	if(!block || !head)
		return -EINVAL;
	if(!block->rx_hook)
		return -EINVAL;

	pthread_mutex_lock(&list_head_lock);
	while((*head) != NULL) {
		if(block->priority > (*head)->priority)
			break;
		head = &((*head)->next);
	}
	block->next = (*head);
	(*head) = block;
	pthread_mutex_unlock(&list_head_lock);

	return 0;
}

static int __register_rx_hook_once(naonet_block_t **head, naonet_block_t *block)
{
	if(!block || !head)
		return -EINVAL;
	if(!block->rx_hook)
		return -EINVAL;

	pthread_mutex_lock(&list_head_lock);
	while((*head) != NULL) {
		if(unlikely(block == (*head)))
			return (-EEXIST);
		if(block->priority > (*head)->priority)
			break;
		head = &((*head)->next);
	}
	block->next = (*head);
	(*head) = block;
	pthread_mutex_unlock(&list_head_lock);

	return 0;
}

static int __unregister_rx_hook(naonet_block_t **head, naonet_block_t *block)
{
	if(!block || !head)
		return -EINVAL;

	pthread_mutex_lock(&list_head_lock);
	while((*head) != NULL) {
		if(unlikely(block == (*head))) {
			(*head) = block->next;
			break;
		}
		head = &((*head)->next);
	}
	pthread_mutex_unlock(&list_head_lock);

	return 0;
}

static int __call_rx_hook_chain(naonet_block_t **head, uint8_t type, 
				uint32_t src, char *message, size_t len,
				int *called)
{
	int rc = BLOCK_SUCC_DONE;
	naonet_block_t *block = (*head), *next_block;

	if(!head || !message || !len)
		return -EINVAL;
	if(called)
		(*called) = 0;

	pthread_mutex_lock(&list_head_lock);
	while(block) {
		next_block = block->next;
		if(block->type != type)
			goto next;
		rc = block->rx_hook(block, src, message, len);
		if((rc & BLOCK_STOP_CHAIN) == BLOCK_STOP_CHAIN)
			break;
		if(called)
			(*called)++;
next:
		block = next_block;
	}
	pthread_mutex_unlock(&list_head_lock);
	return rc;
}

int register_rx_hook(naonet_block_t *block)
{
	return __register_rx_hook(&list_head, block);
}

int register_rx_hook_once(naonet_block_t *block)
{
	return __register_rx_hook_once(&list_head, block);
}

int unregister_rx_hook(naonet_block_t *block)
{
	return __unregister_rx_hook(&list_head, block);
}

static int call_rx_hook_chain(uint8_t type, uint32_t src, char *message, 
			      size_t len, int *called)
{
	return  __call_rx_hook_chain(&list_head, type, src, message, len, 
				     called);
}

int send_message(char *message, size_t len, uint8_t type)
{
	int fd, rc = 0;
	size_t ret;
	char buf[NET_DEV_MAX_SEG_SIZE];

	if(!message || !len || len >= NET_DEV_MAX_SEG_SIZE)
		return -EINVAL;

	fd = open(NET_DEV_FILE, O_WRONLY);
	if(fd < 0) {
		perror("open");
		exit(EXIT_FAILURE);
	}

	/* No memset in fastpath */
	memcpy(&buf[1], message, len);
	buf[0] = type;

	ret = write(fd, buf, len + 1);
	if(ret != (len + 1)) {
		perror("write");
		rc = -EIO;
	}

	close(fd);
	return rc;
}

static void *rx_thread(void *null)
{
	int fd;
	size_t len;
	char buf[NET_DEV_MAX_SEG_SIZE];
	naonet_header_t *nh;

	fd = open(NET_DEV_FILE, O_RDONLY);
	if(fd < 0) {
		perror("open");
		pthread_exit(0);
	}

	while((len = read(fd, buf, sizeof(buf))) > 0) {
		nh = (naonet_header_t *) ((void *) buf);
		call_rx_hook_chain((uint8_t) (*(buf + sizeof(*nh))), nh->src_id,
				   (buf + sizeof(*nh) + 1), ntohs(nh->len) - 1,
				   NULL);
		memset(buf, 0, sizeof(buf));
	}

	close(fd);
	pthread_exit(0);
}

uint32_t naonet_fetch_id(void)
{
	int fd;
	uint32_t id = -1;
	char buff[200] = {0};

	fd = open("/proc/naonet/id", O_RDONLY);
	if(fd < 0) {
		perror("Cannot open /proc/naonet/id");
		return EXIT_FAILURE;
	}

	if(read(fd, buff, sizeof(buff)) > 0) {
		sscanf(buff, "%u", &id);
	}
	close(fd);

	if(id == -1)
		printf("Cannot parse naonet ID from /var/log/messages!\n");
	return id;
}

int naonet_init(pthread_t *rx_thread_p)
{
	int rc;

	if(!rx_thread_p)
		return -EINVAL;

	rc = pthread_create(rx_thread_p, NULL, rx_thread, NULL);
	if(rc) {
		perror("pthread_create");
		exit(EXIT_FAILURE);
        }

	pthread_detach((*rx_thread_p));
	return 0;
}
