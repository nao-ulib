/*
 * nao-ulib
 * Copyright 2011 Daniel Borkmann <dborkma@tik.ee.ethz.ch>
 * Subject to the GPL.
 * Nao-Team HTWK,
 * Faculty of Computer Science, Mathematics and Natural Sciences,
 * Leipzig University of Applied Sciences (HTWK Leipzig)
 *
 * Mostly compiler related stuff.
 * Much code taken from the Linux kernel.  For such code, the option
 * to redistribute under later versions of GPL might not be available.
 */

#ifndef COMP_X86_H
#define COMP_X86_H

#ifndef likely
# define likely(x)          __builtin_expect(!!(x), 1)
#endif

#ifndef unlikely
# define unlikely(x)        __builtin_expect(!!(x), 0)
#endif

#ifndef __deprecated
# define __deprecated       /* unimplemented */
#endif

#ifndef unreachable
# define unreachable()      do { } while (1)
#endif

//#ifndef barrier
//# define barrier()          __sync_synchronize()
//#endif

#define barrier()           __asm__ __volatile__("": : :"memory")
#define mb()                asm volatile("mfence":::"memory")
#define rmb()               asm volatile("lfence":::"memory")
#define wmb()               asm volatile("sfence"::: "memory")
#define smp_mb()            mb()
#define smp_rmb()           rmb()
#define smp_wmb()           wmb()

/* Nop everywhere except on alpha. */
#define smp_read_barrier_depends()

#ifndef bug
# define bug()              __builtin_trap()
#endif

#ifndef bug
# define build_bug_on(condition)  ((void)sizeof(char[1 - 2*!!(condition)]))
#endif

#ifndef bug
# define build_bug_on_zero(e)     (sizeof(char[1 - 2 * !!(e)]) - 1)
#endif

#ifndef mark_unreachable
# define mark_unreachable() __builtin_unreachable()
#endif

#ifndef is_type
# define is_type(x, type)   __builtin_types_compatible_p(typeof(x), (type))
#endif

#ifndef same_type
# define same_type(x, y)    __builtin_types_compatible_p(typeof(x), typeof(y))
#endif

#ifndef __read_mostly
# define __read_mostly      __attribute__((__section__(".data.read_mostly")))
#endif

#ifndef __must_check
# define __must_check       /* unimplemented */
#endif

#ifndef __used
# define __used             /* unimplemented */
#endif

#ifndef __maybe_unused
# define __maybe_unused     /* unimplemented */
#endif

#ifndef __always_unused
# define __always_unused    /* unimplemented */
#endif

#ifndef noinline
# define noinline           __attribute__((noinline))
#endif

#ifndef __always_inline
# define __always_inline    inline
#endif

#ifndef __inline__
# define __inline__         inline
#endif

/*
 * Protected visibility is like default visibility except that it indicates 
 * that references within the defining module will bind to the definition 
 * in that module. That is, the declared entity cannot be overridden by 
 * another module.
 */
#ifndef __protected
# define __protected        __attribute__((visibility("protected")))
#endif

/*
 * Hidden visibility indicates that the entity declared will have a new form 
 * of linkage, which we'll call "hidden linkage". Two declarations of an 
 * object with hidden linkage refer to the same object if they are in the 
 * same shared object.
 */
#ifndef __hidden
# define __hidden           __attribute__((visibility("hidden")))
#endif

/*
 * Internal visibility is like hidden visibility, but with additional 
 * processor specific semantics. Unless otherwise specified by the psABI, 
 * GCC defines internal visibility to mean that a function is never called 
 * from another module. Compare this with hidden functions which, while they 
 * cannot be referenced directly by other modules, can be referenced 
 * indirectly via function pointers. By indicating that a function cannot be 
 * called from outside the module, GCC may for instance omit the load of a 
 * PIC register since it is known that the calling function loaded the 
 * correct value.
 */
#ifndef __internal
# define __internal         __attribute__((visibility("internal")))
#endif

#ifndef max
# define max(a, b)                         \
	({                                 \
		typeof (a) _a = (a);       \
		typeof (b) _b = (b);       \
		_a > _b ? _a : _b;         \
	})
#endif /* max */

#ifndef min
# define min(a, b)                         \
	({                                 \
		typeof (a) _a = (a);       \
		typeof (b) _b = (b);       \
		_a < _b ? _a : _b;         \
	})
#endif /* min */

#ifndef offsetof
# define offsetof(type, member) ((size_t) &((type *) 0)->member)
#endif /* offsetof */

/*
 * Casts a member of a structure out to the containing structure.
 */
#ifndef container_of
# define container_of(ptr, type, member)                              \
	({                                                            \
		const typeof(((type *) 0)->member) * __mptr = (ptr);  \
		(type *) ((char *) __mptr - offsetof(type, member));  \
	})
#endif

/* REP NOP (PAUSE) is a good thing to insert into busy-wait loops. */
static inline void rep_nop(void)
{
	asm volatile("rep; nop" ::: "memory");
}

static inline void cpu_relax(void)
{
	rep_nop();
}

/*
 * Prevent the compiler from merging or refetching accesses.  The compiler
 * is also forbidden from reordering successive instances of ACCESS_ONCE(),
 * but only when the compiler is aware of some particular ordering.  One way
 * to make the compiler aware of ordering is to put the two invocations of
 * ACCESS_ONCE() in different C statements.
 *
 * This macro does absolutely -nothing- to prevent the CPU from reordering,
 * merging, or refetching absolutely anything at any time.  Its main intended
 * use is to mediate communication between process-level code and irq/NMI
 * handlers, all running on the same CPU.
 */
#define ACCESS_ONCE(x) (*(volatile typeof(x) *)&(x))

#endif /* COMP_X86_H */
