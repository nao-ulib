/*
 * nao-ulib
 * Copyright 2011 Daniel Borkmann <dborkma@tik.ee.ethz.ch>
 * Subject to the GPL.
 * Nao-Team HTWK,
 * Faculty of Computer Science, Mathematics and Natural Sciences,
 * Leipzig University of Applied Sciences (HTWK Leipzig)
 */

#define _GNU_SOURCE
#include <sys/types.h>
#include <unistd.h>
#include <sched.h>

#include "die.h"
#include "psched.h"

void run_on_cpu(int cpu)
{
	cpu_set_t mask;

	CPU_ZERO(&mask);
	CPU_SET(cpu, &mask);

	sched_setaffinity(0, sizeof(mask), &mask);
}

int set_proc_prio(int priority)
{
	/*
	 * setpriority() is clever, even if you put a nice value which 
	 * is out of range it corrects it to the closest valid nice value
	 */
	int ret = setpriority(PRIO_PROCESS, getpid(), priority);
	if (ret)
		panic("Can't set nice val to %i!\n", priority);

	return 0;
}

int set_sched_status(int policy, int priority)
{
	int ret, min_prio, max_prio;
	struct sched_param sp;

	max_prio = sched_get_priority_max(policy);
	min_prio = sched_get_priority_min(policy);

	if (max_prio == -1 || min_prio == -1)
		whine("Cannot determine scheduler prio limits!\n");
	else if (priority < min_prio)
		priority = min_prio;
	else if (priority > max_prio)
		priority = max_prio;

	memset(&sp, 0, sizeof(sp));
	sp.sched_priority = priority;

	ret = sched_setscheduler(getpid(), policy, &sp);
	if (ret) {
		whine("Cannot set scheduler policy!\n");
		return -EINVAL;
	}

	ret = sched_setparam(getpid(), &sp);
	if (ret) {
		whine("Cannot set scheduler prio!\n");
		return -EINVAL;
	}

	return 0;
}

