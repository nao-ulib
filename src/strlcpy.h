/*
 * nao-ulib
 * Copyright 2011 Daniel Borkmann <dborkma@tik.ee.ethz.ch>
 * Subject to the GPL.
 * Nao-Team HTWK,
 * Faculty of Computer Science, Mathematics and Natural Sciences,
 * Leipzig University of Applied Sciences (HTWK Leipzig)
 */

#ifndef STRLCPY_H
#define STRLCPY_H

extern size_t strlcpy(char *dest, const char *src, size_t size);

#endif /* STRLCPY_H */
