/*
 * nao-ulib
 * Copyright 2011 Daniel Borkmann <dborkma@tik.ee.ethz.ch>
 * Subject to the GPL.
 * Nao-Team HTWK,
 * Faculty of Computer Science, Mathematics and Natural Sciences,
 * Leipzig University of Applied Sciences (HTWK Leipzig)
 */

#ifndef NOTIFIER_H
#define NOTIFIER_H

#include <stdint.h>
#include <stdio.h>

#define BLOCK_SUCC_DONE   0
#define BLOCK_STOP_CHAIN  1

enum event_prio {
        PRIO_VERYLOW,
        PRIO_LOW,
        PRIO_MEDIUM,
        PRIO_HIGH,
        PRIO_EXTRA,
};

struct event_block {
	enum event_prio prio;
	int (*hook)(const struct event_block *self, unsigned long event,
		    const void *arg);
	struct event_block *next;
};

extern int register_event_hook(struct event_block **head,
			       struct event_block *block);
extern int register_event_hook_once(struct event_block **head,
				    struct event_block *block);
extern int unregister_event_hook(struct event_block **head,
				 struct event_block *block);
extern int call_event_hooks(struct event_block **head, unsigned long event,
			    const void *arg, int *called);

#endif /* NOTIFIER_H */
