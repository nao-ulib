/*
 * nao-ulib
 * Copyright (C) 2009 Daniel Borkmann <borkmann@gnumaniacs.org>
 * Copyright (C) 2009 Thomas Reinhardt <treinhar@imn.htwk-leipzig.de>
 * Copyright (C) 2009 Rico Tilgner <rtilgner@imn.htwk-leipzig.de>
 * Subject to the GPL.
 * Nao-Team HTWK,
 * Faculty of Computer Science, Mathematics and Natural Sciences,
 * Leipzig University of Applied Sciences (HTWK Leipzig)
 */

#ifndef SORT_H
#define SORT_H

extern void sort(float *a, size_t n);

#endif /* SORT_H */
