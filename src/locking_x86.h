/*
 * nao-ulib
 * Copyright 2011 Daniel Borkmann <dborkma@tik.ee.ethz.ch>
 * Subject to the GPL.
 * Nao-Team HTWK,
 * Faculty of Computer Science, Mathematics and Natural Sciences,
 * Leipzig University of Applied Sciences (HTWK Leipzig)
 */

#ifndef LOCKING_X86_H
#define LOCKING_X86_H

#include <pthread.h>

struct nao_spin_lock {
	pthread_spinlock_t lock;
};

struct nao_mutex_lock {
	pthread_mutex_t lock;
};

static inline int nao_spin_lock_init(struct nao_spin_lock *l)
{
	return -pthread_spin_init(&l->lock, 0);
}

static inline void nao_spin_lock_destroy(struct nao_spin_lock *l)
{
	pthread_spin_destroy(&l->lock);
}

static inline void nao_spin_lock_lock(struct nao_spin_lock *l)
{
	pthread_spin_lock(&l->lock);
}

static inline void nao_spin_lock_unlock(struct nao_spin_lock *l)
{
	pthread_spin_unlock(&l->lock);
}

static inline int nao_mutex_lock_init(struct nao_mutex_lock *l)
{
	return -pthread_mutex_init(&l->lock, 0);
}

static inline void nao_mutex_lock_destroy(struct nao_mutex_lock *l)
{
	pthread_mutex_destroy(&l->lock);
}

static inline void nao_mutex_lock_lock(struct nao_mutex_lock *l)
{
	pthread_mutex_lock(&l->lock);
}

static inline void nao_mutex_lock_unlock(struct nao_mutex_lock *l)
{
	pthread_mutex_unlock(&l->lock);
}

#endif /* LOCKING_X86_H */
