nao-ulib

Nao robot userspace library for AMD Geode real-time hacking! This project
aims to create a universal userspace library for the Aldebaran Nao robotic
platform, that is optimized for real-time applications on AMD Geode. It is
developed by members of the Nao-Team HTWK, Faculty of Computer Science,
Mathematics and Natural Sciences from the Leipzig University of Applied 
Sciences and published as Open Source in the hope, that other teams will find
it useful. It's still in the beginning of everything and devloped within our
spare time, so you might check our repository from time to time ... ;-)

Some of the features:

 * Low-level IP-less Nao intercom
 * Real-time memory allocator
 * General utilities and x* wrapper
 * Lockless data structures
 * Atomic variables
 * Event notification framework
 * x86/MMX memcpy
 * Video4Linux mmap interface
 * I2C camera selector
 * RB-Trees, Mergesort
 * Spinlocks, Mutexes
 * Mersenne Twister PRNG
 * Process Scheduling routines
 * RS232 "debugger"
 * Stacktracer
 * Syslog interface
 * Several Bit Hackings
 * Deflate for image compression
 * Aldebaran Kernel Source
 * ...

More to come. Also, if you have some interesting ideas, send your proposals
to the authors (see AUTHORS file). We want you for testing and patching! ;-)

Credits go to all the other members of the Nao-Team HTWK and to Professor
Klaus Bastian as well as to Professor Karl-Udo Jahn for supporting us!

Cheers and happy Nao hacking,
                                            Daniel Borkmann & Tobias Kalbitz
                                            http://naoteam.imn.htwk-leipzig.de/
